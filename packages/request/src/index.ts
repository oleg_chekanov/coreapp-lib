import { Action } from "redux";
import { useDispatch, useSelector } from "react-redux";
import urlcat from "urlcat";
import { takeLatest, call, put } from "redux-saga/effects";

export const REQUEST = "REQUEST";
export const REQUEST_SUCCESS = "REQUEST_SUCCESS";

const request = ({
  name,
  params,
}: {
  name: string;
  params: Record<string, string>;
}) => ({
  type: REQUEST,
  name,
  params,
});

const requestSuccess = ({ result, name }: { result: any; name: string }) => ({
  type: REQUEST_SUCCESS,
  name,
  result,
});

export type RequestConfig = Record<string, { reducer: string; url: string }>;

export const createReducer =
  (requestConfig: RequestConfig) => (state: any, action: Action) => {
    switch (action.type) {
      case REQUEST_SUCCESS: {
        const { name, result } = action as ReturnType<typeof requestSuccess>;

        const reducer = requestConfig[name].reducer;
        return { ...state, [reducer]: result };
      }
      default: {
        return state;
      }
    }
  };

export const createUseRequest = (requestConfig: RequestConfig) => {
  const createSelector = (name: string) => {
    const reducer = requestConfig[name].reducer;

    return (state: any) => state[reducer];
  };

  return (name: string) => {
    const dispatch = useDispatch();

    const call = (params: Record<string, string>) => {
      dispatch(request({ name, params }));
    };

    const selector = createSelector(name);
    const result = useSelector(selector);

    return [result, call];
  };
};

const createRequestSaga = (requestConfig: RequestConfig) => {
  return function* watchRequestSaga() {
    yield takeLatest(REQUEST, function* ({ name, params }) {
      const response = yield call(
        fetch,
        urlcat(requestConfig[name].url, params)
      );

      const result = yield call([response, "json"]);

      yield put(requestSuccess({ result, name }));
    });
  };
};
