import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { setSomeValue } from "./actions/someValueAction";

const App = () => {
  const [value, setValue] = useState("");
  const dispatch = useDispatch();

  const someValue = useSelector<{ someValue: string }>(
    (state) => state.someValue
  );

  return (
    <div>
      <h1>Example page</h1>
      <p>
        <label htmlFor="someValue">Value: </label>
        <input
          id="someValue"
          onChange={({ target }) => {
            setValue(target.value);
          }}
          type="text"
        />
      </p>
      <p>
        <button
          onClick={() => {
            dispatch(setSomeValue({ value }));
          }}
        >
          Set value
        </button>
      </p>
      <p>Some value now is: {someValue}</p>
    </div>
  );
};

export default App;
