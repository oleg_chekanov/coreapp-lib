export const SET_SOME_VALUE = "SET_SOME_VALUE";

export const setSomeValue = ({ value }: { value: string }) => ({
  type: SET_SOME_VALUE,
  payload: { value },
});
