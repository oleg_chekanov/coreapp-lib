import { SET_SOME_VALUE } from "../actions/someValueAction";

const someValueReducer = (state = "", action) => {
  switch (action.type) {
    case SET_SOME_VALUE:
      return action.payload.value;
    default:
      return state;
  }
};

export default someValueReducer;
