import { combineReducers } from "redux";

import someValueReducer from "./someValueReducer";

export default combineReducers({
  someValue: someValueReducer,
});
